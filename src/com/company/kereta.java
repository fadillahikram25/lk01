package com.company;

public class kereta {
    private String namaKereta;
    private int jumlahPenumpang;
    private int jumlahTiket;
    private tiket[] banyakTiket;

    public kereta() {
        namaKereta = "komuter";
        jumlahTiket = 1000;
        banyakTiket = new tiket[jumlahTiket];
    }


    public kereta(String namaKereta, int jumlahTiket) {
        this.namaKereta = namaKereta;
        this.jumlahTiket = jumlahTiket;
        banyakTiket = new tiket[jumlahTiket];
    }


    public void tambahTiket(String namaPenumpang) {
        if (jumlahTiket > jumlahPenumpang) {
            tiket ticket = new tiket(namaPenumpang);
            banyakTiket[jumlahPenumpang] = ticket;
            jumlahPenumpang++;
            System.out.println("========================================================");
            if ((jumlahTiket - jumlahPenumpang) >= 30) {
                System.out.println("Tiket berhasil dipesan");
            } else if ((jumlahTiket - jumlahPenumpang) < 30) {
                System.out.println("Tiket berhasil dipesan. Sisa tiket tersedia :" + (jumlahTiket - jumlahPenumpang));

            }
        } else {
            System.out.println("======================================================================");
            System.out.println("Kereta telah habis dipesan, Silahkan cari jadwal keberangkatan lainnya");
        }
    }


    public int getJumlahTiket() {
        return jumlahTiket;
    }

    public void setJumlahTiket(int jumlahTiket) {
        this.jumlahTiket = jumlahTiket;
    }

    public void tambahTiket(String namaPenumpang, String asal, String tujuan) {
        if (jumlahTiket > jumlahPenumpang) {
            tiket ticket = new tiket(namaPenumpang, asal, tujuan);
            banyakTiket[jumlahPenumpang] = ticket;
            jumlahPenumpang++;
            System.out.println("========================================================");
            if ((jumlahTiket - jumlahPenumpang) >= 30) {
                System.out.println("Tiket berhasil dipesan");
            } else if ((jumlahTiket - jumlahPenumpang) < 30) {
                System.out.println("Tiket berhasil dipesan. Sisa tiket tersedia :" + (jumlahTiket - jumlahPenumpang));

            }
        } else {
            System.out.println("========================================================");
            System.out.println("Kereta telah habis dipesan, Silahkan cari jadwal keberangkatan lainnya");
        }
    }

    public void tampilkanTiket() {
        System.out.println("========================================================");
        System.out.println("Daftar penumpang kereta api " + namaKereta + ":");
        System.out.println("-----------------------------");
        for (int a = 0; a < jumlahPenumpang; a++) {
            //Tiket kereta KAJJ
            if (banyakTiket[a].getAsal() != null) {
                System.out.println("Nama\t: " + banyakTiket[a].getNamaPenumpang());
                System.out.println("Asal\t: " + banyakTiket[a].getAsal());
                System.out.println("Tujuan\t: " + banyakTiket[a].getTujuan());
                System.out.println("-----------------------------");
            }

            //Tiket kereta Komuter
            else {
                System.out.println("Nama\t: " + banyakTiket[a].getNamaPenumpang());
            }
        }
    }
}










