package com.company;

public class tiket {
    private String namaPenumpang;
    private String asal;
    private String tujuan;

    public tiket(String namaPenumpang) {
        this.namaPenumpang = namaPenumpang;
    }

    public tiket(String namaPenumpang, String asal, String tujuan) {
        this.namaPenumpang = namaPenumpang;
        this.asal = asal;
        this.tujuan = tujuan;
    }

    public String getNamaPenumpang() {
        return namaPenumpang;
    }

    public void setNamaPenumpang(String namaPenumpang) {
        this.namaPenumpang = namaPenumpang;
    }

    public String getAsal() {
        return asal;
    }

    public void setAsal(String asal) {
        this.asal = asal;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

}


