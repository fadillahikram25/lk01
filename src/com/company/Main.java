package com.company;

public class Main {

    public static void main(String[] args) {
        kereta komuter = new kereta();
        komuter.tambahTiket("Ahmad Python");
        komuter.tambahTiket("Junaedi Kotlin");
        komuter.tambahTiket("Saiful HTML");
        komuter.tampilkanTiket();

        //KAJJ memiliki parameter nama kereta dan jumlah tiket tersedia.
        kereta KAJJ = new kereta("Jayabaya", 2);
        KAJJ.tambahTiket("Vania Malinda", "Malang", "Surabaya Gubeng");
        KAJJ.tambahTiket("Sekar SD", "Malang", "Sidoarjo");
        KAJJ.tambahTiket("Bonaventura", "Malang", "Surabaya Pasarturi");
        KAJJ.tampilkanTiket();

    }



}
